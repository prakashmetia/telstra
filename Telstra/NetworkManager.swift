//
//  NetworkManager.swift
//  Telstra
//
//  Created by Prakash on 25/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import UIKit
import Foundation

class NetworkManager {
    
    
    /**
     Function to manage API call and ger response
     - Parameter requestURL: Url for the http request
     */
    class func callAPI(requestURL:String?, completionBlock:@escaping((Data?,Error?)->())) {
        
        if Reachability.isConnectedToNetwork() {
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            var config      : URLSessionConfiguration!
            var urlSession  : URLSession!
            
            config     = URLSessionConfiguration.default
            config.timeoutIntervalForRequest = 30
            urlSession = URLSession(configuration: config, delegate: nil, delegateQueue: OperationQueue.main)
            
            let HTTPMethod_Get   =  "POST"
            let callURL = URL.init(string: requestURL!) 
            
            var request = URLRequest.init(url: callURL!)
            //request.setValue("application/text", forHTTPHeaderField: "Content-Type")  // the request is JSON
            //request.setValue("application/text", forHTTPHeaderField: "Accept")
            request.timeoutInterval  = 30.0//TimeOutInterval in Second
            request.cachePolicy      = URLRequest.CachePolicy.reloadIgnoringCacheData
            request.httpMethod       = HTTPMethod_Get
            
            
            
            let dataTask = urlSession.dataTask(with: (request)) { (data,response,error) in
                if error != nil{
                    completionBlock(nil,error)
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                }
                
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                completionBlock(data!,error)
            }
            dataTask.resume()
            
        } else {
            
            print("Check your internet connection...")
            let alertC = UIAlertController(title: "Alert", message: "Please check your internet connection...", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .cancel) { (alert) in
                let err = Error.self
                completionBlock(nil, err as? Error)
            }
            alertC.addAction(okAction)
            UIApplication.shared.keyWindow?.rootViewController?.present(alertC, animated: true, completion: nil)
        }
        
    }
    
}
