//
//  DataCell.swift
//  Telstra
//
//  Created by Prakash on 25/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import UIKit

class DataCell: UITableViewCell {

    
    var dataInfo : DataModel? {
        didSet {
            productNameLabel.text = dataInfo?.title
            productDescriptionLabel.text = dataInfo?.desc
            if !(dataInfo?.imageUrl.isEmpty)! {
                print(dataInfo?.imageUrl ?? "")
                productImage.imageFromServerURL((dataInfo?.imageUrl)!, placeHolder: nil)
            }
        }
    }
    
    //var dataInfo : DataModel?
    
    private let productNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    private let productDescriptionLabel : UITextView = {
        let lbl = UITextView()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 13)
        lbl.textAlignment = .left
        //lbl.numberOfLines = 0
        lbl.isScrollEnabled = false
        lbl.isEditable = false
        lbl.clipsToBounds = true
        return lbl
    }()
    
    private let productImage : UIImageView = {
        let imgView = UIImageView(image:nil)
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(productDescriptionLabel)
        contentView.addSubview(productNameLabel)
        contentView.addSubview(productImage)
        
        productNameLabel.translatesAutoresizingMaskIntoConstraints = false
        productDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        productImage.translatesAutoresizingMaskIntoConstraints = false
        let marginGuide = contentView.layoutMarginsGuide
        
        productImage.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        productImage.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        productImage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        productImage.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
        productNameLabel.leadingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: 113).isActive = true
        productNameLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 5).isActive = true
        productNameLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        productNameLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        productDescriptionLabel.leadingAnchor.constraint(equalTo: productImage.leadingAnchor, constant: 110).isActive = true
        productDescriptionLabel.topAnchor.constraint(equalTo: productNameLabel.bottomAnchor).isActive = true
        productDescriptionLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        productDescriptionLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        productDescriptionLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 70).isActive = true
                
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    
    func imageFromServerURL(_ URLString: String, placeHolder: UIImage?) {
        
        self.image = nil
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            self.image = cachedImage
            return
        }
        
        if let url = URL(string: URLString) {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil {
                    DispatchQueue.main.async {
                        self.image = placeHolder
                    }
                    return
                }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            self.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}



