//
//  DataTest.swift
//  TelstraTests
//
//  Created by Prakash on 26/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import XCTest
@testable import Telstra

class DataTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    
    // 1. test case to check internet connection
    func testTelstraUnitTest() {
        XCTAssertTrue(Reachability.isConnectedToNetwork())
    }
    
    
    // 2. test case to check if API response has no data
    func testForAPIResponse() {
        
        let modelManager = ViewModel()
        let expec = expectation(description: "data found")
        modelManager.dataRequest {
            expec.fulfill()
            XCTAssertTrue(modelManager.dataArray.count != 0)
        }
        wait(for: [expec], timeout: 30)
        
    }
    
    
    // 3. test case to check if API response 14 data of expected format
    func testForAPIResponseDataCount() {
        
        let modelManager = ViewModel()
        let expec = expectation(description: "data found")
        modelManager.dataRequest {
            expec.fulfill()
            XCTAssertEqual(modelManager.dataArray.count, 14)
        }
        wait(for: [expec], timeout: 30)
        
    }
    
    
    // 4. test cast to check if data is missing from imageUrl or title or desc
    func testForAPIResponseObjectData() {
        
        let modelManager = ViewModel()
        let expec = expectation(description: "data found")
        modelManager.dataRequest {
            
            expec.fulfill()
            for apiData in modelManager.dataArray {
                XCTAssertEqual(apiData.imageUrl, "")
                XCTAssertEqual(apiData.title, "")
                XCTAssertEqual(apiData.desc, "")
            }
            
        }
        
        wait(for: [expec], timeout: 30)
        
    }
    
}
