//
//  ViewModel.swift
//  Telstra
//
//  Created by Prakash on 25/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import Foundation

class ViewModel: NSObject {
    
    let urlToRequest = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    var dataArray = [DataModel]()
    
    /**
     Function to call API and parse the response data accordingly
     */
    func dataRequest(requestHandler:@escaping(()->())) {
        
        NetworkManager.callAPI(requestURL: urlToRequest) {[weak self] (data:Data?, error:Error?) in
            
            self?.dataArray = [DataModel]()
            
            if data == nil || error != nil {
                requestHandler()
                return
            }
            
            let resString = String(data: data!, encoding: String.Encoding.isoLatin1)
            let resData = resString!.data(using: String.Encoding.utf8)
            
            do {
                
                let jsonData = try JSONSerialization.jsonObject(with: resData!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String:Any]
                
                if let rows = jsonData["rows"] {
                    for row in (rows as! Array<Dictionary<String, Any>>) {
                        self?.dataArray.append(DataModel.init(dataObj: row))
                    }
                }
                requestHandler()
                
            }
            catch { print("Data parse error: ", error.localizedDescription) }
            
        }
        
    }
    
}
