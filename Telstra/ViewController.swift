//
//  ViewController.swift
//  Telstra
//
//  Created by Prakash on 25/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    private var dataTable : UITableView = {
        let tbl = UITableView()
        tbl.frame = CGRect.zero
        tbl.isHidden = true
        return tbl
    }()
    
    
    let cellID = "cellID"
    let modelManager = ViewModel()
    let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.modelManager.dataRequest(requestHandler: {[weak self] in
                
                self?.dataTable.isHidden = false
                self?.dataTable.reloadData()
                if (self?.refreshControl.isRefreshing)! {
                    self?.refreshControl.endRefreshing()
                }
                
            })
        }
        
        
    }
    
    override func loadView() {
        super.loadView()
        
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        dataTable.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /**
     Function to be called on Pull to Refresh
     */
    @objc func refreshTable() {
        
        self.refreshControl.beginRefreshing()
        self.modelManager.dataRequest(requestHandler: {[weak self] in
            
            self?.refreshControl.endRefreshing()
            self?.dataTable.isHidden = false
            self?.dataTable.reloadData()
            
        })
        
    }
    
    /**
     Function to create UI elements dynamically and manage their layouts
     */
    func createUI() {
        
        dataTable.dataSource = self
        dataTable.delegate = self
        dataTable.estimatedRowHeight = 100
        dataTable.rowHeight = UITableViewAutomaticDimension
        
        dataTable.register(DataCell.self, forCellReuseIdentifier: cellID)
        view.addSubview(dataTable)
        refreshControl.addTarget(self, action: #selector(refreshTable), for: .valueChanged)
        dataTable.addSubview(refreshControl)
        dataTable.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraints([
            NSLayoutConstraint(item: dataTable, attribute: .leading, relatedBy: .equal, toItem: self.view, attribute: .leading, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: dataTable, attribute: .trailing, relatedBy: .equal, toItem: self.view, attribute: .trailing, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: dataTable, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: dataTable, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1.0, constant: 25),
            ])
        
    }
        
    
    //MARK UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelManager.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataCell = dataTable.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! DataCell
        dataCell.dataInfo = modelManager.dataArray[indexPath.row]
        return dataCell
    }
            
}

