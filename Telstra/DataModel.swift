//
//  DataModel.swift
//  Telstra
//
//  Created by Prakash on 25/10/18.
//  Copyright © 2018 Wipro. All rights reserved.
//

import Foundation

class DataModel: NSObject {
    
    var title = ""
    var desc = ""
    var imageUrl = ""
    
    init(dataObj: [String:Any]) {
        super.init()
        
        if let _title = dataObj["title"], !(_title is NSNull) {
            title = _title as! String
        }
        if let _desc = dataObj["description"], !(_desc is NSNull) {
            desc = _desc as! String
        }
        if let _imageUrl = dataObj["imageHref"], !(_imageUrl is NSNull) {
            imageUrl = _imageUrl as! String
        }
        
    }
    
}
